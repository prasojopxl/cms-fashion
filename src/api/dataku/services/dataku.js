'use strict';

/**
 * dataku service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::dataku.dataku');
