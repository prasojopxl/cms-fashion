'use strict';

/**
 * dataku controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::dataku.dataku');
