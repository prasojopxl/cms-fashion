'use strict';

/**
 * dataku router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::dataku.dataku');
