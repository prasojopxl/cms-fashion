module.exports= {
    async afterCreate(event) {
        const {result} = event;
        try {
            await strapi.plugins["email"].services.email.send({
                to: "mail@pejuangkoding.com",
                from: "mail@pejuangkoding.com",
                subject:"You have a new todo",
                html:`<b>Your todo is:</b> ${result.title}`
            })
        } catch (err) {
            console.log(err)
        }
    }
}